.. _apireference:

=============
API reference
=============

* :ref:`run_api`
* :ref:`configuration_api`
* :ref:`datastructures_api`
* :ref:`detectors_api`
* :ref:`detectorproperties_api`
* :ref:`pipelines_api`
* :ref:`exposure_api`
* :ref:`observation_api`
* :ref:`calibration_api`
* :ref:`inputs_api`
* :ref:`outputs_api`
* :ref:`notebook_api`
* :ref:`util_api`

.. toctree::
   :caption: api reference
   :maxdepth: 1
   :hidden:

   api/run.rst
   api/configuration.rst
   api/datastructures.rst
   api/detectors.rst
   api/detectorproperties.rst
   api/pipelines.rst
   api/exposure.rst
   api/observation.rst
   api/calibration.rst
   api/inputs.rst
   api/outputs.rst
   api/notebook.rst
   api/util.rst
